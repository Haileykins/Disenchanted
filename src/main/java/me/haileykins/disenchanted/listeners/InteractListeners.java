package me.haileykins.disenchanted.listeners;

import me.haileykins.disenchanted.common.DisenchantMenu;
import me.haileykins.disenchanted.handlers.ConfigHandler;
import me.haileykins.disenchanted.handlers.LanguageHandler;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class InteractListeners implements Listener {

    private DisenchantMenu disenchantMenu;
    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;

    public InteractListeners(DisenchantMenu disenchantMenu, ConfigHandler configHandler, LanguageHandler languageHandler) {
        this.disenchantMenu = disenchantMenu;
        this.configHandler = configHandler;
        this.languageHandler = languageHandler;
    }

    @EventHandler
    private void onInteract(PlayerInteractEvent event) {
        Material blockType = Material.valueOf(configHandler.getConfig().getString("disenchanting-block"));
        if (event.getClickedBlock() == null || event.getClickedBlock().getType() != blockType) {
            return;
        }

        if (event.getAction() != Action.LEFT_CLICK_BLOCK || event.getPlayer().isSneaking() || event.getItem() == null) {
            return;
        }

        if (configHandler.getConfig().getStringList("banned-items").contains(event.getItem().getType().toString())) {
            event.getPlayer().sendMessage(languageHandler.getMessage("banned-item", true));
            return;
        }

        if (event.getItem().getItemMeta() == null || !event.getItem().getItemMeta().hasEnchants()) {
            return;
        }

        if (!event.getPlayer().hasPermission("disenchanted.disenchant")) {
            event.getPlayer().sendMessage(languageHandler.getMessage("no-permission", true));
            return;
        }

        event.getPlayer().openInventory(disenchantMenu.open(event.getItem()));
    }

}
