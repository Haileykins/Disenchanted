package me.haileykins.disenchanted.listeners;

import me.haileykins.disenchanted.common.Utilities;
import me.haileykins.disenchanted.handlers.ConfigHandler;
import me.haileykins.disenchanted.handlers.EconomyHandler;
import me.haileykins.disenchanted.handlers.LanguageHandler;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class InventoryListeners implements Listener {

    private ConfigHandler configHandler;
    private EconomyHandler economyHandler;
    private LanguageHandler languageHandler;

    public InventoryListeners(ConfigHandler configHandler, EconomyHandler economyHandler, LanguageHandler languageHandler) {
        this.configHandler = configHandler;
        this.economyHandler = economyHandler;
        this.languageHandler = languageHandler;
    }

    @EventHandler
    private void inventoryClick(InventoryClickEvent event) {
        if (event.getClickedInventory() == null || event.getCurrentItem() == null) {
            return;
        }

        if (!event.getView().getTitle().equals(languageHandler.getMessage("menu-title", false))) {
            return;
        }

        event.setCancelled(true);

        Player player = (Player) event.getWhoClicked();
        ItemStack item = event.getCurrentItem();

        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);
        EnchantmentStorageMeta meta = (EnchantmentStorageMeta) book.getItemMeta();

        Enchantment enchant = null;

        for (Enchantment enchantment : item.getEnchantments().keySet()) {

            assert meta != null : " Meta is Null!";
            meta.addStoredEnchant(enchantment, item.getEnchantments().get(enchantment), false);
            enchant = enchantment;
        }

        if (enchant == null) {
            return;
        }

        List<String> disabledEnchants = configHandler.getConfig().getStringList("disabled-enchants");

        if (disabledEnchants.contains(Utilities.getCommonName(enchant).toLowerCase())) {
            player.sendMessage(languageHandler.getMessage("banned-enchant", true));
            return;
        }

        economyHandler.handlePurchase(enchant, item, player, book, meta);
    }

    @EventHandler
    private void onClose(InventoryCloseEvent event) {
        if (!event.getView().getTitle().equalsIgnoreCase(languageHandler.getMessage("menu-title", false))) {
            return;
        }

        Arrays.stream(event.getInventory().getContents())
                .filter(Objects::nonNull)
                .filter(item -> item.getType() != Material.ENCHANTED_BOOK)
                .forEach(event.getInventory()::addItem);
    }
}
