package me.haileykins.disenchanted.common;

import me.haileykins.disenchanted.handlers.ConfigHandler;
import me.haileykins.disenchanted.handlers.EconomyHandler;
import me.haileykins.disenchanted.handlers.LanguageHandler;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collections;

public class DisenchantMenu {

    private ConfigHandler configHandler;
    private EconomyHandler economyHandler;
    private LanguageHandler languageHandler;

    public DisenchantMenu(ConfigHandler configHandler, EconomyHandler economyHandler, LanguageHandler languageHandler) {
        this.configHandler = configHandler;
        this.economyHandler = economyHandler;
        this.languageHandler = languageHandler;
    }

    public Inventory open(ItemStack item) {
        Inventory inventory = Bukkit.createInventory(null, 18, languageHandler.getMessage("menu-title", false));
        FileConfiguration config = configHandler.getConfig();
        boolean useEconomy = config.getBoolean("use-economy");
        String symbol = useEconomy ? languageHandler.getMessage("economy-symbol", false) : languageHandler.getMessage("xp-symbol", false);

        if (!configHandler.getConfig().getBoolean("use-economy")) {
            symbol = languageHandler.getMessage("xp-symbol", false);
        }

        int slot = 0;

        for (Enchantment enchantment : item.getEnchantments().keySet()) {
            ItemStack book = new ItemStack(Material.ENCHANTED_BOOK);
            ItemMeta meta = book.getItemMeta();

            assert meta != null : " Meta For Book Is Null";

            meta.addEnchant(enchantment, item.getEnchantmentLevel(enchantment), true);

            int cost = economyHandler.getEnchantmentCost(enchantment, item.getEnchantments().get(enchantment));

            String formattedCost = useEconomy ? symbol + cost : cost + symbol;

            meta.setLore(Collections.singletonList(formattedCost));
            book.setItemMeta(meta);
            inventory.setItem(slot, book);

            slot++;
        }

        return inventory;
    }
}
