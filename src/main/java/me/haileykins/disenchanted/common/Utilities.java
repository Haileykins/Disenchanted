package me.haileykins.disenchanted.common;

import org.bukkit.enchantments.Enchantment;

public class Utilities {

    public static String getCommonName(Enchantment enchantment) {
        return enchantment.getKey().getKey();
    }
}
