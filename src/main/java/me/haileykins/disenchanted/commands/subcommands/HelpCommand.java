package me.haileykins.disenchanted.commands.subcommands;

import me.haileykins.disenchanted.commands.CommandBase;
import me.haileykins.disenchanted.handlers.LanguageHandler;
import org.bukkit.command.CommandSender;

public class HelpCommand implements CommandBase {

    private LanguageHandler languageHandler;

    public HelpCommand(LanguageHandler languageHandler) {
        this.languageHandler = languageHandler;
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (sender.hasPermission("disenchanted.admin")) {
            sender.sendMessage(languageHandler.getMessage("help-reload", false));
        }

        sender.sendMessage(languageHandler.getMessage("help-disenchant", false));
        sender.sendMessage(languageHandler.getMessage("help-help", false));
    }

    @Override
    public String getCommandName() {
        return "help";
    }

    @Override
    public String getPermissionNode() {
        return "";
    }
}
