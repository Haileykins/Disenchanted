package me.haileykins.disenchanted.commands.subcommands;

import me.haileykins.disenchanted.commands.CommandBase;
import me.haileykins.disenchanted.handlers.ConfigHandler;
import me.haileykins.disenchanted.handlers.LanguageHandler;
import org.bukkit.command.CommandSender;

public class ReloadCommand implements CommandBase {

    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;

    public ReloadCommand(ConfigHandler configHandler, LanguageHandler languageHandler) {
        this.configHandler = configHandler;
        this.languageHandler = languageHandler;
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (args.length <= 1) {
            configHandler.loadConfig();
            sender.sendMessage(languageHandler.getMessage("config-reloaded", true));
            languageHandler.loadLang();
            sender.sendMessage(languageHandler.getMessage("lang-reloaded", true));
            return;
        }

        if (args[1].equalsIgnoreCase("config")) {
            configHandler.loadConfig();
            sender.sendMessage(languageHandler.getMessage("config-reloaded", true));
            return;
        }

        if (args[1].equalsIgnoreCase("lang")) {
            languageHandler.loadLang();
            sender.sendMessage(languageHandler.getMessage("lang-reloaded", true));
            return;
        }

        sender.sendMessage(languageHandler.getMessage("file-doesnt-exist", true));
    }

    @Override
    public String getCommandName() {
        return "reload";
    }

    @Override
    public String getPermissionNode() {
        return "disenchanted.admin";
    }
}
