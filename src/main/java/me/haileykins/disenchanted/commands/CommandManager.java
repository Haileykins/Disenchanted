package me.haileykins.disenchanted.commands;

import me.haileykins.disenchanted.common.DisenchantMenu;
import me.haileykins.disenchanted.handlers.LanguageHandler;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import javax.sound.midi.SysexMessage;
import java.util.HashSet;
import java.util.Set;

public class CommandManager implements CommandExecutor {

    private DisenchantMenu disenchantMenu;
    private LanguageHandler languageHandler;

    public CommandManager(DisenchantMenu disenchantMenu, LanguageHandler languageHandler) {
        this.disenchantMenu = disenchantMenu;
        this.languageHandler = languageHandler;
    }

    private final Set<CommandBase> commands = new HashSet<>();

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {


        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage(languageHandler.getMessage("must-be-player", true));
                return true;
            }

            Player player = (Player) sender;

            if (!player.hasPermission("disenchanted.disenchant.remote")) {
                player.sendMessage(languageHandler.getMessage("no-permission", true));
                return true;
            }


            ItemStack item = player.getInventory().getItemInMainHand();

            if (item.getType() != Material.AIR) {
                player.openInventory(disenchantMenu.open(item));
            }

            return true;
        }

        final CommandBase command = getCommand(args[0]);

        if (command == null) {
            sender.sendMessage(languageHandler.getMessage("invalid-command", true));
            return true;
        }

        final String permission = command.getPermissionNode();

        if (command.hasPermissionNode() && !sender.hasPermission(permission)) {
            sender.sendMessage(languageHandler.getMessage("no-permission", true));
            return true;
        }
        
        command.onCommand(sender, args);
        return true;
    }

    public void registerCommand(CommandBase commandBase) {
        commands.add(commandBase);
    }

    private CommandBase getCommand(String command) {
        for (CommandBase commandBase : commands) {
            if (commandBase.getCommandName().equalsIgnoreCase(command)) {
                return commandBase;
            }
        }
        return null;
    }
}
