package me.haileykins.disenchanted.commands;

import org.bukkit.command.CommandSender;

public interface CommandBase {
    void onCommand(CommandSender sender, String[] args);
    String getCommandName();
    String getPermissionNode();

    default boolean hasPermissionNode() {
        return getPermissionNode() != null && !getPermissionNode().isEmpty();
    }
}
