package me.haileykins.disenchanted;

import me.haileykins.disenchanted.commands.CommandManager;
import me.haileykins.disenchanted.commands.subcommands.HelpCommand;
import me.haileykins.disenchanted.commands.subcommands.ReloadCommand;
import me.haileykins.disenchanted.common.DisenchantMenu;
import me.haileykins.disenchanted.handlers.ConfigHandler;
import me.haileykins.disenchanted.handlers.EconomyHandler;
import me.haileykins.disenchanted.handlers.LanguageHandler;
import me.haileykins.disenchanted.listeners.InteractListeners;
import me.haileykins.disenchanted.listeners.InventoryListeners;
import me.haileykins.disenchanted.listeners.UpdateListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public final class Disenchanted extends JavaPlugin {

    @Override
    public void onEnable() {
        ConfigHandler configHandler = new ConfigHandler(this);
        LanguageHandler languageHandler = new LanguageHandler(this);
        EconomyHandler economyHandler = new EconomyHandler(configHandler, languageHandler, this);
        DisenchantMenu disenchantMenu = new DisenchantMenu(configHandler, economyHandler, languageHandler);

        getServer().getPluginManager().registerEvents(new InteractListeners(disenchantMenu, configHandler, languageHandler), this);
        getServer().getPluginManager().registerEvents(new InventoryListeners(configHandler, economyHandler, languageHandler), this);
        getServer().getPluginManager().registerEvents(new UpdateListener(this, configHandler, languageHandler), this);

        CommandManager commandManager = new CommandManager(disenchantMenu, languageHandler);
        commandManager.registerCommand(new HelpCommand(languageHandler));
        commandManager.registerCommand(new ReloadCommand(configHandler, languageHandler));

        Objects.requireNonNull(getCommand("disenchanted")).setExecutor(commandManager);

        configHandler.loadConfig();
        languageHandler.loadLang();

        if (!economyHandler.setupEconomy()) {
            getLogger().info("No Vault Found. Resorting To XP Purchases!");
            configHandler.getConfig().set("use-economy", false);
            configHandler.saveConfig();
            configHandler.loadConfig();
        }

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
