package me.haileykins.disenchanted.handlers;

import me.haileykins.disenchanted.common.Utilities;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

public class EconomyHandler {

    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;
    private Plugin plugin;

    private Economy economy = null;

    public EconomyHandler(ConfigHandler configHandler, LanguageHandler languageHandler, Plugin plugin) {
        this.configHandler = configHandler;
        this.languageHandler = languageHandler;
        this.plugin = plugin;
    }

    public boolean setupEconomy() {
        if (plugin.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }

        RegisteredServiceProvider<Economy> rsp = plugin.getServer().getServicesManager().getRegistration(Economy.class);

        if (rsp == null) {
            return false;
        }

        economy = rsp.getProvider();
        return true;
    }

    public void handlePurchase(Enchantment enchantment, ItemStack item, Player player, ItemStack book, ItemMeta meta) {
        int cost = getEnchantmentCost(enchantment, item.getEnchantments().get(enchantment));

        if (configHandler.getConfig().getBoolean("require-book-and-quill")) {
            ItemStack writableBook = new ItemStack(Material.WRITABLE_BOOK, 1);

            if (!player.getInventory().containsAtLeast(writableBook, 1)) {
                player.sendMessage(languageHandler.getMessage("writable-book-required", true));
                return;
            }

            player.getInventory().removeItem(new ItemStack(Material.WRITABLE_BOOK, 1));
        }

        if (player.getInventory().firstEmpty() == -1) {
            player.sendMessage(languageHandler.getMessage("no-inv-space", true));
            return;
        }

        if (configHandler.getConfig().getBoolean("use-economy")) {
            if (!economy.has(player, cost)) {
                player.sendMessage(languageHandler.getMessage("cant-afford", true));
                return;
            }

            economy.withdrawPlayer(player, cost);
        } else {
            if (player.getLevel() < cost) {
                player.sendMessage(languageHandler.getMessage("cant-afford", true));
                return;
            }

            player.setLevel(player.getLevel() - cost);
        }

        book.setItemMeta(meta);

        player.getInventory().addItem(book);
        player.getInventory().getItemInMainHand().removeEnchantment(enchantment);
        player.sendMessage(languageHandler.getMessage("purchase-successful", true));
        player.closeInventory();
    }

    public int getEnchantmentCost(Enchantment enchantment, int level) {
        FileConfiguration config = plugin.getConfig();
        ConfigurationSection enchants = config.getConfigurationSection("enchantments");

        assert enchants != null : " Config Section \"enchantments\" is missing!";

        return enchants.getInt(Utilities.getCommonName(enchantment).toLowerCase() + "." + level);
    }
}
