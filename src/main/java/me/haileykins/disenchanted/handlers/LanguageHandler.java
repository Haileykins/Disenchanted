package me.haileykins.disenchanted.handlers;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class LanguageHandler {

    private FileConfiguration config;
    private Plugin plugin;

    public LanguageHandler(Plugin plugin) {
        this.plugin = plugin;
    }

    public void loadLang() {
        File file = new File(plugin.getDataFolder(), "lang.yml");

        if (!file.exists()) {
            plugin.getLogger().info("No Language File Found, Creating...");
            plugin.saveResource("lang.yml", false);
        }
        config = YamlConfiguration.loadConfiguration(file);
    }

    public void saveLang() {
        File file = new File(plugin.getDataFolder(), "lang.yml");

        try {
            config.save(file);
        } catch (IOException e) {
            plugin.getLogger().severe("Lang File Failed To Save!");
        }
    }

    public String color(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public String getMessage(String key, boolean prefixed) {
        if (prefixed) {
            return color(config.getString("prefix") + " " + config.getString(key));
        }

        return color(config.getString(key));
    }
}
