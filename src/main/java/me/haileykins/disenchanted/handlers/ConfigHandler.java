package me.haileykins.disenchanted.handlers;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class ConfigHandler {

    private FileConfiguration config;
    private Plugin plugin;

    public ConfigHandler(Plugin plugin) {
        this.plugin = plugin;
    }

    public void loadConfig() {
        File file = new File(plugin.getDataFolder(), "config.yml");

        if (!file.exists()) {
            plugin.getLogger().info("No Config File Found, Creating...");
            plugin.saveResource("config.yml", false);
        }
        config = YamlConfiguration.loadConfiguration(file);

        saveConfig();
    }

    public void saveConfig() {
        File file = new File(plugin.getDataFolder(), "config.yml");

        try {
            config.save(file);
        } catch (IOException e) {
            plugin.getLogger().severe("Config File Failed To Save!");
        }
    }


    public FileConfiguration getConfig() {
        return config;
    }
}
